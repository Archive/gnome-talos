/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright (C) 2011 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Colin Walters <walters@verbum.org>
 */

#include "config.h"

#include <gio/gio.h>
#include <gio/gunixinputstream.h>

#include <string.h>
#include <stdio.h>

#include "gnome-talos-procinfo.h"

typedef void (WalkProcCallback) (long pid, const char *prefix, gpointer user_data);

static void
walk_proc (WalkProcCallback callback, gpointer user_data)
{
  GFile *proc;
  GFileEnumerator *iter;
  GError *error = NULL;
  GRegex *pid_regex;

  pid_regex = g_regex_new ("[0-9]+", 0, 0, NULL);

  proc = g_file_new_for_path ("/proc");

  iter = g_file_enumerate_children (proc, "standard::*",
				    G_FILE_QUERY_INFO_NONE,
				    NULL,
				    &error);
  if (!iter)
    g_error ("%s", error->message);

  while (TRUE)
    {
      GFileInfo *fileinfo;
      const char *name;
      GMatchInfo *match;
      char *fullpath;

      fileinfo = g_file_enumerator_next_file (iter, NULL, &error);
      if (error != NULL)
	{
	  g_clear_error (&error);
	  continue;
	}
      if (!fileinfo)
	break;

      fullpath = g_build_filename ("/proc", g_file_info_get_name (fileinfo), NULL);

      name = g_file_info_get_display_name (fileinfo);

      if (g_regex_match (pid_regex, name, 0, &match))
	{
	  long pid = (long) g_ascii_strtoll (name, NULL, 10);
	  callback (pid, fullpath, user_data);
	}

      g_free (fullpath);
      g_object_unref (fileinfo);
    }

  g_object_unref (iter);
  g_object_unref (proc);

  g_regex_unref (pid_regex);
}

typedef struct {
  GVariantBuilder *builder;
} AcquireMemData; 

static char*
proc_get_procname (const char *proc_prefix)
{
  char *filename;
  char *result = NULL;
  char *contents;
  gsize len;
  
  filename = g_build_filename (proc_prefix, "cmdline", NULL);
  if (!g_file_get_contents (filename, &contents, &len, NULL))
    goto out;
  if (memchr (contents, 0, len) == NULL)
    {
      g_free (contents);
      goto out;
    }
    
  result = contents;

 out:
  g_free (filename);
  
  return result;
}
		   

static void
proc_get_process_size (const char *proc_prefix,
		       gulong     *vm_size,
		       gulong     *rss_size)
{
  char *filename;
  char *contents;
  char *iter;
  gsize len;
  int i;

  *vm_size = *rss_size = 0;

  filename = g_build_filename (proc_prefix, "stat", NULL);

  if (!g_file_get_contents (filename, &contents, &len, NULL))
    goto out;

  iter = contents;
  /* See "man proc" for where this 22 comes from */
  for (i = 0; i < 22; i++) {
    iter = strchr (iter, ' ');
    if (!iter)
      goto out;
    iter++;
  }
  sscanf (iter, " %lu", vm_size);
  iter = strchr (iter, ' ');
  if (iter)
    sscanf (iter, " %lu", rss_size);

 out:
  g_free (contents);
  g_free (filename);
}

static void
proc_get_uidgid (const char *proc_prefix,
		 gint32     *real_uid,
		 gint32     *effective_uid,
		 gint32     *real_gid,
		 gint32     *effective_gid)
{
  char *filename;
  char *contents;
  char *uid;
  char *gid;
  gsize len;

  filename = g_build_filename (proc_prefix, "status", NULL);

  if (!g_file_get_contents (filename, &contents, &len, NULL))
    goto out;

  uid = strstr (contents, "\nUid:");
  if (!uid)
    goto out;
  uid += 5;

  *real_uid = (gint32) g_ascii_strtoll (uid, 
					&uid,
					10);

  *effective_uid = (gint32) g_ascii_strtoll (uid, 
					     NULL,
					     10);
  
  gid = strstr (contents, "\nGid:");
  if (!gid)
    goto out;

  *real_gid = (gint32) g_ascii_strtoll (gid, 
					&gid,
					10);

  *effective_gid = (gint32) g_ascii_strtoll (gid, 
					     &gid,
					     10);
  
 out:
  g_free (filename);
}


static void
acquire_mem_iter (long          pid,
		  const char   *proc_prefix,
		  gpointer      user_data)
{
  AcquireMemData *data = user_data;
  GVariantBuilder *builder = data->builder;
  char *procname;
  gulong vm_size;
  gulong rss_size;
  gint32 real_uid, effective_uid, real_gid, effective_gid;

  procname = proc_get_procname (proc_prefix);

  if (procname == NULL)  /* Probably a kernel thread */
    return;
  
  proc_get_process_size (proc_prefix, &vm_size, &rss_size); 
  proc_get_uidgid (proc_prefix, &real_uid, &effective_uid, &real_gid, &effective_gid);

  g_variant_builder_open (builder, G_VARIANT_TYPE ("{uv}"));
  g_variant_builder_add (builder, "u", (guint32)pid);

  g_variant_builder_open (builder, G_VARIANT_TYPE_VARIANT);
  g_variant_builder_open (builder, G_VARIANT_TYPE_ARRAY);

  g_variant_builder_add (builder, "{sv}", "argv0", g_variant_new_string (procname));
  g_free (procname);
  g_variant_builder_add (builder, "{sv}", "real-uid", g_variant_new_int32 (real_uid));
  if (real_uid != effective_uid)
    g_variant_builder_add (builder, "{sv}", "effective-uid", g_variant_new_int32 (effective_uid));
  g_variant_builder_add (builder, "{sv}", "rss", g_variant_new_int64 (rss_size));
  g_variant_builder_add (builder, "{sv}", "vm", g_variant_new_int64 (vm_size));

  g_variant_builder_close (builder);
  g_variant_builder_close (builder);
  g_variant_builder_close (builder);
}

GVariant *
gnome_talos_acquire_procinfo (void)
{
  GVariantBuilder builder;
  AcquireMemData data;

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("(a{uv})"));
  data.builder = &builder;

  g_variant_builder_open (&builder, G_VARIANT_TYPE ("a{uv}"));

  walk_proc (acquire_mem_iter, &data);

  g_variant_builder_close (&builder);

  return g_variant_builder_end (&builder);
}

