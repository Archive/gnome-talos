/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright (C) 2011 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Colin Walters <walters@verbum.org>
 */

#include "config.h"

#include <glib-unix.h>
#include <gio/gio.h>
#include <json-glib/json-glib.h>

#include <string.h>

#include "gnome-talos-procinfo.h"
#include "gnome-talos-gpu.h"
#include "gnome-talos-meta.h"

#include "report.html.h"

static void
fatal_gerror (GError **error) G_GNUC_NORETURN;

static void
fatal_gerror (GError **error)
{
  g_assert (*error != NULL);
  g_printerr ("%s\n", (*error)->message);

  exit (1);
}

static gint timeout = 5;
static char *log_file_path = NULL;
static char *to_html_file_path = NULL;

static GVariant *
gather_snapshot (void)
{
  GError *error = NULL;
  gint64 start_timestamp, end_timestamp;
  GVariant *proc_data;
  GVariant *gpu_data;
  GVariantBuilder builder;
  GVariant *data;

  start_timestamp = g_get_monotonic_time ();

  proc_data = gnome_talos_acquire_procinfo ();
  gpu_data = gnome_talos_acquire_gpu_snapshot (&error);
  if (gpu_data == NULL)
    fatal_gerror (&error);

  end_timestamp = g_get_monotonic_time ();

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("(a{sv}a{sv})"));

  /* metadata */
  g_variant_builder_open (&builder, G_VARIANT_TYPE ("a{sv}"));
  g_variant_builder_add (&builder, "{sv}",
			 "start-timestamp",
			 g_variant_new_int64 (start_timestamp));
  g_variant_builder_add (&builder, "{sv}",
			 "end-timestamp",
			 g_variant_new_int64 (end_timestamp));
  g_variant_builder_close (&builder);

  /* data */
  g_variant_builder_open (&builder, G_VARIANT_TYPE ("a{sv}"));

  g_variant_builder_add (&builder, "{sv}", "processes", proc_data);
  g_variant_builder_add (&builder, "{sv}", "gpu", gpu_data);

  g_variant_builder_close (&builder);

  data = g_variant_builder_end (&builder);
 
  return data;
}

static gboolean
write_variant (GVariant       *variant,
	       GCancellable   *cancellable,
	       GOutputStream  *out,
	       GError        **error)
{
  JsonNode *node;
  JsonGenerator *generator;
  gboolean ret;

  generator = json_generator_new ();
  json_generator_set_pretty (generator, TRUE);

  node = json_gvariant_serialize (variant);
  json_generator_set_root (generator, node);
  ret = json_generator_to_stream (generator, out, cancellable, error);
 
  g_object_unref (generator);
  json_node_free (node);

  return ret;
}

typedef struct {
  GFile *log_file;
  GFileOutputStream *out;
  GMainLoop *loop;
} GnomeTalosApp;

static gboolean
timeout_gather_data (gpointer user_data)
{
  GnomeTalosApp *app = user_data;
  GVariant *snapshot;
  GError *error = NULL;

  snapshot = gather_snapshot ();
  if (!write_variant (snapshot, NULL, (GOutputStream*)app->out, &error))
    fatal_gerror (&error);
  g_variant_unref (snapshot);

  if (timeout > 0)
    g_timeout_add_seconds (timeout, timeout_gather_data, app);
  else
    g_main_loop_quit (app->loop);

  return FALSE;
}

static gboolean
on_interrupt_received (gpointer user_data)
{
  GnomeTalosApp *app = user_data;
  GError *error = NULL;

  if (!g_output_stream_write_all ((GOutputStream*)app->out,
				  "]", 1, NULL, NULL, &error))
    fatal_gerror (&error);

  if (!g_output_stream_close ((GOutputStream*)app->out, NULL, &error))
    fatal_gerror (&error);
  
  g_main_loop_quit (app->loop);

  return FALSE;
}

static void
run_collection (GnomeTalosApp *app)
{
  GError *error = NULL;
  GVariantBuilder builder;
  GVariant *meta;
  GVariant *sysinfo;
  GVariant *gpu_meta;

  g_unix_signal_add (SIGINT,
                     on_interrupt_received,
                     app);
  g_unix_signal_add (SIGTERM,
                     on_interrupt_received,
                     app);

  app->out = g_file_replace (app->log_file, NULL, FALSE,
                             G_FILE_CREATE_REPLACE_DESTINATION,
                             NULL, &error);
  if (app->out == NULL)
    fatal_gerror (&error);

  if (!g_output_stream_write_all ((GOutputStream*)app->out,
                                  "[", 1, NULL, NULL, &error))
    fatal_gerror (&error);

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{sv}"));

  sysinfo = gnome_talos_acquire_meta ();
  g_variant_builder_add (&builder, "{sv}",
                         "system",
                         sysinfo);

  gpu_meta = gnome_talos_acquire_gpu_meta (&error);
  if (!gpu_meta)
    fatal_gerror (&error);
  g_variant_builder_add (&builder, "{sv}",
                         "gpu",
                         gpu_meta);

  meta = g_variant_builder_end (&builder);
  if (!write_variant (meta, NULL, (GOutputStream*)app->out, &error))
    fatal_gerror (&error);
  g_variant_unref (meta);

  if (!g_output_stream_write_all ((GOutputStream*)app->out,
                                  ",\n", 2, NULL, NULL, &error))
    fatal_gerror (&error);

  g_idle_add (timeout_gather_data, app);
}

static char *
replace_key (char *buf,
             const char *key,
             const char *value)
{
  GRegex *regex;
  char *escaped_key;
  char *key_regex;
  char *new_buf;

  escaped_key = g_regex_escape_string (key, -1);
  key_regex = g_strconcat ("@", escaped_key, "@", NULL);
  g_free (escaped_key);

  regex = g_regex_new (key_regex, 0, 0, NULL);
  g_assert (regex != NULL);

  new_buf = g_regex_replace (regex, buf, -1, 0, value, 0, NULL);
  g_free (buf);

  g_regex_unref (regex);

  return new_buf;
}
             
static void
convert_to_html (GnomeTalosApp *app)
{
  GError *error = NULL;
  JsonParser *parser;
  JsonGenerator *generator;
  JsonNode *root;
  GFile *to_html_file;
  GFileOutputStream *out;
  char *data_string;
  char *buf;

  parser = json_parser_new ();

  if (!json_parser_load_from_file (parser, log_file_path, &error))
    fatal_gerror (&error);

  root = json_parser_get_root (parser);

  to_html_file = g_file_new_for_path (to_html_file_path);

  if ((out = g_file_replace (to_html_file, NULL, FALSE,
                             G_FILE_CREATE_REPLACE_DESTINATION,
                             NULL, &error)) == NULL)
    fatal_gerror (&error);

  generator = json_generator_new ();
  json_generator_set_pretty (generator, TRUE);
  json_generator_set_root (generator, root);
  data_string = json_generator_to_data (generator, NULL);
  g_object_unref (generator);
  json_node_free (root);

  buf = g_strdup (REPORT_HTML);
  buf = replace_key (buf, "SRC_FILE", log_file_path);
  buf = replace_key (buf, "JQUERY_PATH", JQUERY_PATH);
  buf = replace_key (buf, "JQUERY_FLOT_PATH", JQUERY_FLOT_PATH);
  buf = replace_key (buf, "DATA", data_string);
  g_free (data_string);

  if (!g_output_stream_write_all ((GOutputStream*)out,
                                  buf, strlen (buf), NULL, NULL, &error))
    fatal_gerror (&error);
  g_free (buf);

  if (!g_output_stream_close ((GOutputStream*)out, NULL, &error))
    fatal_gerror (&error);

  g_idle_add ((GSourceFunc)g_main_loop_quit, app->loop);
}

int
main (int    argc,
      char **argv)
{
  GnomeTalosApp app;
  GError *error = NULL;
  GOptionContext *context;
  static const GOptionEntry options[] = {
    { "timeout", 0, 0, G_OPTION_ARG_INT, &timeout, "Time between data snapshots, in seconds (default=5)", "seconds" },
    { "log-file", 0, 0, G_OPTION_ARG_FILENAME, &log_file_path, "Log file path", "path" },
    { "to-html-file", 0, 0, G_OPTION_ARG_FILENAME, &to_html_file_path, "Convert log file to HTML", "path" },
    { NULL }
  };

  g_type_init ();
  
  context = g_option_context_new (NULL);

  g_option_context_add_main_entries (context, options, NULL);
  
  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return 1;
    }
  if (log_file_path == NULL)
    {
      g_printerr ("--log-file is required\n");
      return 1;
    }

  app.loop = g_main_loop_new (NULL, TRUE);
  app.log_file = g_file_new_for_path (log_file_path);

  if (to_html_file_path == NULL)
    run_collection (&app);
  else
    convert_to_html (&app);

  g_main_loop_run (app.loop);
  
  return 0;
}
