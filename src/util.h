/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright (C) 2011 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Colin Walters <walters@verbum.org>
 */

#ifndef __GNOME_TALOS_UTIL__
#define __GNOME_TALOS_UTIL__ 1

#include <glib.h>

char *
talos_util_build_filename_get_utf8_contents_sync (GError **error,
						  const char *arg0,
						  ...) G_GNUC_NULL_TERMINATED;

char *
talos_util_get_file_contents_utf8_sync (const char *path,
                                        GError    **error);

GVariant *
talos_util_rfc822_file_to_variant (const char *path,
                                   GError    **error);

#endif
