/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright (C) 2011 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Colin Walters <walters@verbum.org>
 */

#include "config.h"

#include <gio/gio.h>

#include <string.h>
#include <stdio.h>

#include "gnome-talos-gpu.h"
#include "util.h"

#define GPU_BASE_PATH "/sys/kernel/debug/dri"

static char *gpudir;
static gboolean is_i915;
static GRegex *i915_gem_objects_regex;

static void
init_gpu_dir_unlocked (void)
{
  const char *filename;
  GDir *dir;
  GSList *names = NULL;
  GSList *iter;

  dir = g_dir_open (GPU_BASE_PATH, 0, NULL);
  if (!dir)
    {
      gpudir = NULL;
      return;
    }

  while ((filename = g_dir_read_name (dir)) != NULL)
    {
      names = g_slist_prepend (names, g_strdup (filename));
    }
  g_dir_close (dir);

  for (iter = names; iter; iter = iter->next)
    {
      gboolean name_exists;
      char *name;
      char *contents;

      name = g_build_filename (GPU_BASE_PATH, iter->data, "name", NULL);
      if (g_file_test (name, G_FILE_TEST_EXISTS)
          && g_file_get_contents (name, &contents, NULL, NULL))
        {
          name_exists = strlen (contents) > 0;
          g_free (contents);
        }
      else
        name_exists = FALSE;
      g_free (name);

      if (name_exists)
        {
          gpudir = g_build_filename (GPU_BASE_PATH, iter->data, NULL);
          break;
        }
    }

  for (iter = names->next; iter; iter = iter->next)
    {
      if (iter->data != gpudir)
        g_free (iter->data);
    }
}


static void
init_gpu_static (void)
{
  static gsize statics_initialized = 0;
  char *filename;

  if (!g_once_init_enter (&statics_initialized))
    return;

  init_gpu_dir_unlocked ();

  filename = g_build_filename (gpudir, "i915_capabilities", NULL);
  is_i915 = g_file_test (filename, G_FILE_TEST_EXISTS);
  g_free (filename);

  i915_gem_objects_regex = g_regex_new ("([0-9]+) objects, ([0-9]+) bytes", 0, 0, NULL);
  
  g_once_init_leave (&statics_initialized, 1); 
}


GVariant *
gnome_talos_acquire_gpu_meta (GError **error)
{
  char *name;
  GVariantBuilder builder;

  init_gpu_static ();

  if (gpudir == NULL)
    return NULL;

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{sv}"));
  
  name = talos_util_build_filename_get_utf8_contents_sync (error,
                                                           gpudir,
                                                           "name",
                                                           NULL);
  if (!name)
    goto failed;
  /* Kill trailing newline */
  (void)g_strchomp (name);

  g_variant_builder_add (&builder, "{sv}",
			 "name",
			 g_variant_new_string (name));
  g_free (name);

  if (is_i915)
    {
      GVariantBuilder i915_builder;
      GVariant *i915_capabilities;
      char *filename;

      filename = g_build_filename (gpudir, "i915_capabilities", NULL);
      i915_capabilities = talos_util_rfc822_file_to_variant (filename, error);
      g_free (filename);
      if (!i915_capabilities)
        goto failed;

      g_variant_builder_init (&i915_builder, G_VARIANT_TYPE ("a{sv}"));

      g_variant_builder_add (&i915_builder, "{sv}",
                             "capabilities",
                             g_variant_new_variant (i915_capabilities));

      g_variant_builder_add (&builder, "{sv}",
                             "i915",
                             g_variant_new_variant (g_variant_builder_end (&i915_builder)));
    }

  return g_variant_builder_end (&builder);

 failed:
  g_variant_builder_clear (&builder);
  return NULL;
}

static gboolean
i915_gpu_snapshot (GVariantBuilder *builder,
                   GError         **error)
{
  char *contents;
  GMatchInfo *match;

  contents = talos_util_build_filename_get_utf8_contents_sync (error, gpudir, "i915_gem_objects", NULL);
  if (!contents)
    return FALSE;
  
  if (g_regex_match (i915_gem_objects_regex, contents, 0, &match))
    {
      char *count;
      
      count = g_match_info_fetch (match, 1);
      g_variant_builder_add (builder, "{sv}",
                             "gem-object-count",
                             g_variant_new_int64 (g_ascii_strtoll (count, NULL, 10)));
      g_free (count);
      count = g_match_info_fetch (match, 2);
      g_variant_builder_add (builder, "{sv}",
                             "gem-object-bytes",
                             g_variant_new_int64 (g_ascii_strtoll (count, NULL, 10)));
      g_free (count);
      g_match_info_free (match);
    }

  return TRUE;
}

GVariant *
gnome_talos_acquire_gpu_snapshot (GError **error)
{
  GVariantBuilder builder;

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{sv}"));

  if (is_i915)
    {
      GVariantBuilder i915_builder;

      g_variant_builder_init (&i915_builder, G_VARIANT_TYPE ("a{sv}"));

      if (!i915_gpu_snapshot (&i915_builder, error))
        {
          g_variant_builder_clear (&i915_builder);
          goto failed;
        }

      g_variant_builder_add (&builder, "{sv}",
                             "i915",
                             g_variant_new_variant (g_variant_builder_end (&i915_builder)));
    }

  return g_variant_builder_end (&builder);
  
 failed:
  g_variant_builder_clear (&builder);
  return NULL;
}
