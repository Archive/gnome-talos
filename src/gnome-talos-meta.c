/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright (C) 2011 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Colin Walters <walters@verbum.org>
 */

#include "config.h"

#include <gio/gio.h>

#include "gnome-talos-meta.h"

#include <string.h>

GVariant *
gnome_talos_acquire_meta (void)
{
  GVariantBuilder builder;
  GDBusConnection *system_bus;
  GError *error = NULL;
  char *checksum;
  char *procdata;
  gsize len;

  system_bus = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &error);
  if (!system_bus)
    g_error ("%s", error->message);
 
  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{sv}"));

  checksum = g_compute_checksum_for_string (G_CHECKSUM_SHA256,
					    g_dbus_connection_get_guid (system_bus),
					    -1);
  g_variant_builder_add (&builder, "{sv}",
			 "uuid",
			 g_variant_new_string (checksum));
  g_free (checksum);

  g_variant_builder_add (&builder, "{sv}",
			 "version",
			 g_variant_new_string (GIT_VERSION));
  g_variant_builder_add (&builder, "{sv}",
			 "mypid",
			 g_variant_new_int64 ((gint64)getpid ()));
  g_variant_builder_add (&builder, "{sv}",
			 "myuid",
			 g_variant_new_int32 ((gint32)getuid ()));

  if (g_file_get_contents ("/proc/meminfo", &procdata, &len, NULL))
    {
      const char *memtotal_str = strstr (procdata, "MemTotal:");

      if (memtotal_str)
	{
	  guint64 memtotal;

	  memtotal = g_ascii_strtoll (memtotal_str + strlen ("MemTotal:"), NULL, 10);

	  memtotal *= 1024;

	  g_variant_builder_add (&builder, "{sv}",
				 "memtotal",
				 g_variant_new_uint64 (memtotal));
	}
    }

  return g_variant_builder_end (&builder);
}
