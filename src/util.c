/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*-
 *
 * Copyright (C) 2011 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Colin Walters <walters@verbum.org>
 */

#include "config.h"

#include "util.h"

#include <gio/gio.h>

#include <string.h>

char *
talos_util_build_filename_get_utf8_contents_sync (GError **error,
                                                  const char *filename,
                                                  ...)
{
  GPtrArray *filenames;
  va_list args;
  char *full_filename;
  char *content;

  va_start (args, filename);

  filenames = g_ptr_array_new ();
  g_ptr_array_add (filenames, (char*)filename);

  while ((filename = va_arg (args, const char*)) != NULL)
    g_ptr_array_add (filenames, (char*)filename);
  
  g_ptr_array_add (filenames, NULL);
  full_filename = g_build_filenamev ((char**)filenames->pdata);

  g_ptr_array_free (filenames, TRUE);

  va_end (args);
  
  content = talos_util_get_file_contents_utf8_sync (full_filename, error);

  g_free (full_filename);
  
  return content;
}

/**
 * talos_util_get_file_contents_utf8_sync:
 * @path: UTF-8 encoded filename path
 * @error: a #GError
 *
 * Synchronously load the contents of a file as a NUL terminated
 * string, validating it as UTF-8.  Embedded NUL characters count as
 * invalid content.
 *
 * Returns: (transfer full): File contents
 */
char *
talos_util_get_file_contents_utf8_sync (const char *path,
                                        GError    **error)
{
  char *contents;
  gsize len;
  if (!g_file_get_contents (path, &contents, &len, error))
    return NULL;
  if (!g_utf8_validate (contents, len, NULL))
    {
      g_free (contents);
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "File %s contains invalid UTF-8",
                   path);
      return NULL;
    }
  return contents;
}

GVariant *
talos_util_rfc822_file_to_variant (const char *path,
                                   GError    **error)
{
  GVariantBuilder builder;
  GVariant *res = NULL;
  GFile *file;
  GFileInputStream *in = NULL;
  GDataInputStream *datain = NULL;
  char *line = NULL;
  GError *temp_error = NULL;

  file = g_file_new_for_path (path);
  in = g_file_read (file, NULL, error);
  g_object_unref (file);
  if (!in)
    return NULL;
  datain = g_data_input_stream_new ((GInputStream*)in);

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{sv}"));

  while ((line = g_data_input_stream_read_line_utf8 (datain, NULL, NULL, &temp_error)) != NULL)
    {
      char *colon;
      colon = strchr (line, ':');
      if (colon)
        {
          char *key;
          char *value;

          key = line;
          *colon = '\0';
          value = colon + 1;
          g_strstrip (key);
          g_strstrip (value);

          g_variant_builder_add (&builder, "{sv}",
                                 key, g_variant_new_string (value));
        }
      g_free (line);
      line = NULL;
    }

  if (temp_error != NULL)
    {
      g_free (line);
      g_variant_builder_clear (&builder);
      g_propagate_error (error, temp_error);
      goto out;
    }
  else
    res = g_variant_builder_end (&builder);

 out:
  g_object_unref (in);
  g_object_unref (datain);
  return res;
}

